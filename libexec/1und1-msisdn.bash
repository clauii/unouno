source 'libexec/platform_id.bash'

function __1und1_msisdn__cleanup {
  [[ "${fifoname:-}" ]] && rm "${fifoname}"
  [[ "${fifodir:-}" ]] && rmdir "${fifodir}"
}

export -f __1und1_msisdn__cleanup

function __1und1_msisdn__load_msisdn {
  local pid exitstatus fifodir fifoname

  trap __1und1_msisdn__cleanup EXIT INT HUP TERM
  fifodir="$(mktemp -d)"
  fifoname="${fifodir}/1und1_msisdn.$RANDOM.fifo"
  mkfifo -m0600 "${fifoname}" || return 1

  (__1und1_msisdn__print_msisdn "$@") > "${fifoname}" &
  pid="$!"
  IFS=$'\t' read -d $'\n' msisdn < "${fifoname}" \
    || true

  wait "${pid}" || exitstatus="$?"

  if [[ ! "${exitstatus}" && ! "${msisdn}" ]]; then
    exitstatus=2
  fi

  __1und1_msisdn__cleanup
  trap - EXIT INT HUP TERM
  return "${exitstatus:-0}"
}

export -f __1und1_msisdn__load_msisdn

function __1und1_msisdn__check_connectivity {
  local platform_id
  local target_url

  target_url='center.vodafone.de'
  if ! platform_id="$(_platform_id)"; then
    echo >&2 'Unable to determine platform'
    return 1
  fi

  case "${platform_id}" in
    'MACOS')
      route -n get -net "${target_url}" >/dev/null
      ;;
    'LINUX' | 'WIN')
      dig "${target_url}" +short \
        | xargs -n 1 ip route get >/dev/null 2>&1
      ;;
    *)
      echo >&2 "Invalid platform ID: ${platform_id}"
      return 1
      ;;
  esac
}

export -f __1und1_msisdn__check_connectivity

function __1und1_msisdn__print_msisdn {
  curl -sS \
    'https://center.vodafone.de/vfcenter/index.html' \
    | sed -nE \
      -e 's/^.*\?msisdn=([-0-9A-Z_a-z]+).*$/\1/p ; t quit' \
      -e 'b' -e ': quit' -e 'q'
}

export -f __1und1_msisdn__print_msisdn

function __1und1_msisdn {
  local msisdn

  __1und1_msisdn__load_msisdn
  echo "${msisdn}"
}

export -f __1und1_msisdn
