source 'libexec/1und1-msisdn.bash'

function __1und1_quota__cleanup {
  [[ "${fifoname:-}" ]] && rm "${fifoname}"
  [[ "${fifodir:-}" ]] && rmdir "${fifodir}"
}

export -f __1und1_quota__cleanup

function __1und1_quota__load_quota {
  local pid exitstatus fifodir fifoname

  trap __1und1_quota__cleanup EXIT INT HUP TERM
  fifodir="$(mktemp -d)"
  fifoname="${fifodir}/1und1_quota.$RANDOM.fifo"
  mkfifo -m0600 "${fifoname}" || return 1

  (__1und1_quota__print_quota "$@") > "${fifoname}" &
  pid="$!"
  IFS=$'\n' read -d '' data_used_mb quota_mb < "${fifoname}" \
    || true

  wait "${pid}" || exitstatus="$?"

  __1und1_quota__cleanup
  trap - EXIT INT HUP TERM
  return "${exitstatus:-0}"
}

export -f __1und1_quota__load_quota

function __1und1_quota__print_quota {
  local msisdn
  # local unouno_cookie_jar
  msisdn="${1?}"
  # unouno_cookie_jar="${2?}"
  curl -sLS \
    "https://mobile.kundenshop.1und1.de/mobile-center`
      `?mno=dnetz&msisdn=${msisdn}" \
    | sed -nE \
      -e 's/^.*<h2 class="mc-headline-a3">([0-9]+).*$/\1/p' \
      -e 's/^.*id="maxHighSpeedVolumeCount">([0-9]+).*$/\1/p' \
    | head -2
}

export -f __1und1_quota__print_quota

function __1und1_quota {
  local msisdn verbose

  set -e

  # unouno_temp_dir="$(mktemp -d)"
  # unouno_cookie_jar="${unouno_temp_dir}/1und1-$RANDOM.txt"

  # echo >&2 'Drawing 1&1 cookie'
  # curl -sS -c "${unouno_cookie_jar}" \
  #   'https://mobile.kundenshop.1und1.de/mobile-center'

  if [[ "${verbose:-0}" -ne 0 ]]; then
    echo >&2 'Checking connectivity'
  fi
  __1und1_msisdn__check_connectivity

  if [[ "${verbose:-0}" -ne 0 ]]; then
    echo >&2 'Querying MSISDN'
  fi
  __1und1_msisdn__load_msisdn

  if [[ "${verbose:-0}" -ne 0 ]]; then
    echo >&2 'Querying 1&1 volume quota'
  fi
  __1und1_quota__load_quota "${msisdn}"

  printf '%d %% used\n' \
    "$(bc -l <<< "scale=0; ${data_used_mb}*100/${quota_mb}")"
}

export -f __1und1_quota
