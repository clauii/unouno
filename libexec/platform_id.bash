_platform_id () {
  local uname_prefixes=('MINGW32' 'Linux' 'Darwin')
  local uname_value="$(uname -s)"
  local platform_ids=('WIN' 'LINUX' 'MACOS')
  local i

  if [[ "${uname_value}" == 'CYGWIN'* ]]; then
    echo 'WIN'
    return 0
  fi

  for(( i=0; i<${#uname_prefixes[@]}; i++ )); do
    if [[ "${uname_value}" == ${uname_prefixes[i]}* ]]; then
      echo "${platform_ids[i]}"
      return 0
    fi
  done

  return 1
}

export -f _platform_id
